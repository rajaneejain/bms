class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  before_create :create_account_no
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  validates :full_name, presence: true  
  validates :contact_no, numericality: {only_integer: true }

  has_many :transactions

  def create_account_no
    self.account_no = 12.times.map{rand(0..9)}.join    
  end  

  def self.withdrawal(user,balance,account_no)
    user.current_bal = (user.current_bal -= balance.to_f).round(2)
    user.save!
    debit = user.transactions.build
    debit.user_id = user.id
    debit.account_no = account_no
    debit.debit_amount = balance
    debit.credit_amount = 0.0
    debit.save!
  end

  def self.deposit(user,balance,account_no)
    user.current_bal = (user.current_bal += balance.to_f).round(2)
    user.save!
    credit = user.transactions.build
    credit.user_id = user.id
    credit.account_no = account_no
    credit.credit_amount = balance
    credit.debit_amount = 0.0
    credit.save!
  end 

end
