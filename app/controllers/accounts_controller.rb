class AccountsController < ApplicationController
  def index 
    @transactions = Transaction.where(user_id: current_user.id)
  end	

  def create_deposit
    if params[:user][:current_bal].to_i <= 0
      flash[:error] = 'Transaction failed! Amount must be greater than 0.00'
      render :deposit
    else 
      @user = User.find_by_account_no(params[:user][:account_no])
      unless @user.blank?
        User.deposit(@user,params[:user][:current_bal],params[:user][:account_no])
        if current_user
          flash[:success] = "Money deposited successfully"
          redirect_to accounts_path
        else
          flash[:success] = "Money deposited successfully and Will get total balance in ammount field"
          render template: 'accounts/deposit' 
        end 
      else
        flash[:error] = "Invalid Account No."
        render :deposit
      end  
    end 
  end


  def create_withdraw
    if params[:user][:current_bal].to_i <= 0
      flash[:error] = 'Transaction failed! Amount must be greater than 0.00'
      render :withdraw
    else 
      @user = User.find(current_user.id) 
      if @user.current_bal <= 0
        flash[:error] = 'Transaction failed! No balance in your account please deposit first'
        render :transfer
      else 
        User.withdrawal(@user,params[:user][:current_bal],params[:user][:account_no])
        flash[:success] = "Money withdrawed successfully"
        redirect_to accounts_path
      end
    end
  end


  def create_transfer
    if params[:user][:current_bal].to_i <= 0
      flash[:error] = 'Transaction failed! Amount must be greater than 0.00'
      render :transfer
    else
      current_user_account = User.find(current_user.id)
      if current_user_account.current_bal <= 0
        flash[:error] = 'Transaction failed! No balance in your account please deposit first'
        render :transfer
      else  
        recipient_account = User.find_by_account_no(params[:user][:account_no])
        User.withdrawal(current_user_account,params[:user][:current_bal],current_user_account.account_no)
        User.deposit(recipient_account,params[:user][:current_bal],params[:user][:account_no])
        flash[:success] = "Money transfered successfully"
        redirect_to accounts_path
      end
    end
  end

end
