class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
    	t.integer :user_id
    	t.bigint :account_no
    	t.decimal :credit_amount
    	t.decimal :debit_amount

      t.timestamps
    end
  end
end
